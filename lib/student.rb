class Student
  #attr_reader :courses, :first_name, :last_name
  #attr_write

  attr_accessor :first_name,:last_name,:courses

  def initialize(first,last)
    @first_name = first
    @last_name = last
    @courses = []
  end

  def name
    @first_name + " " + @last_name
  end

  def has_conflict?(new_course )
    @courses.each do |course|
      return true if course.conflicts_with?(new_course)
    end
    false
  end

  def enroll(course)
    raise Exception.new("course conflicts with already enrolled course") if has_conflict?(course)
    if (!@courses.include? course) && (!course.students.include? self)
      @courses << course
      course.students << self
    end
  end

  def course_load
    h = Hash.new(0)
    @courses.each do |course|
      h[course.department] += course.credits
    end
    h
  end
end
